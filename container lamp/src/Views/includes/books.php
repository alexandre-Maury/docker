
<section class="columns is-multiline is-centered"> 

<?php foreach ($databooks as $book) { ?>

  <div class="card m-1 column is-three-quarters-mobile is-two-thirds-tablet is-half-desktop is-one-third-widescreen is-one-quarter-fullhd">
  	<div class="card-image">
    		<figure class="image is-4by3">
      			<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
    		</figure>
  	</div>
  	<div class="card-content">
    		<div class="media">
      			<div class="media-left">
        			<figure class="image is-48x48">
          				<img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
        			</figure>
      			</div>
      			<div class="media-content">
        			<p class="title is-4"><?php echo $book->book_title . "<br>"; ?> </p>
        			<p class="subtitle is-6">@ <?php echo $book->book_author . " - " . $book->book_category; ?> </p>

      			</div>
    		</div>

    		<div class="content">
	  		<?php	echo $book->book_synopsis . "<br>"; ?>
      			<br>

      			<time datetime="2016-1-1"> <?php echo $book->book_release_date . "<br>"; ?> </time>
    		</div>
  	</div>
  </div>

<?php } ?>

</section> 