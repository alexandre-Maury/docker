<section class="columns is-centered">
  <form class="column is-3" action="register" method="post" >
    <div class="column">
        <label for="lastname">Nom</label>
        <input class="input is-primary" type="text" placeholder="Votre Nom" name="lastname" required>
    </div>
    <div class="column">
        <label for="firstname">Prenom</label>
        <input class="input is-primary" type="text" placeholder="Votre Prenom" name="firstname" required>
    </div>
    <div class="column">
        <label for="email">Email</label>
        <input class="input is-primary" type="text" placeholder="Votre Email" name="email" required>
    </div>
    <div class="column">
        <label for="password">Password</label>
        <input class="input is-primary" type="password" placeholder="Votre Password" name="password" required>
        <input type="checkbox"> I agree to the <a href="#" class="has-text-primary">termes et conditions</a>
    </div>
    <div class="column">
        <button type="submit" class="button is-primary is-fullwidth" >register</button>
    </div>
  </form>
</section>