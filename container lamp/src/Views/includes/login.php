<section class="columns is-centered">
  <form class="column is-3" action="login" method="post" >
    <div class="column">
        <label for="email">Email</label>
        <input class="input is-primary" type="text" placeholder="Email address" name="email" required>
    </div>
    <div class="column">
        <label for="Name">Mot de passe</label>
        <input class="input is-primary" type="password" placeholder="Password" name="password" required> 
        <a href="forget.html" class="is-size-7 has-text-primary">Mot de passe oublié ?</a>
    </div>
    <div class="column">
        <button type="submit" class="button is-primary is-fullwidth" >Se connecter</button>
    </div>
  </form>
</section>
