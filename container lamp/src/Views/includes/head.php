<!-- Meta Tags -->
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- En-têtes de sécurité HTTP -->
<meta http-equiv="Strict-Transport-Security" content="max-age=31536000; includeSubDomains">
<meta http-equiv="Content-Security-Policy" content="default-src 'self'; script-src 'self' https://cdn.jsdelivr.net; style-src 'self' https://cdn.jsdelivr.net;">
<meta http-equiv="X-Frame-Options" content="DENY">
<meta http-equiv="X-XSS-Protection" content="1; mode=block">
<meta http-equiv="X-Content-Type-Options" content="nosniff">
<meta name="referrer" content="no-referrer">
<meta http-equiv="Feature-Policy" content="camera 'none'; microphone 'none'">

<meta name="author" content="<?= WEBSITE_AUTHOR?>">
<meta name="description" content="<?= WEBSITE_DESCRIPTION?>" />
<meta name="keywords" content="<?= WEBSITE_KEYWORDS?>"/>
<meta name="Reply-to" content="<?= WEBSITE_AUTHOR_MAIL?>">
<meta name="Copyright" content="<?= WEBSITE_AUTHOR?>">
<meta name="Language" content="<?= WEBSITE_LANGUAGE?>">

<!-- Open Graph tags -->
<meta property="og:type"              content="website" />
<meta property="og:url"               content="<?= WEBSITE_FACEBOOK_URL?>" />
<meta property="og:title"             content="<?= WEBSITE_FACEBOOK_NAME?>" />
<meta property="og:description"       content="<?= WEBSITE_FACEBOOK_DESCRIPTION?>" />
<meta property="og:image"             content="<?= WEBSITE_FACEBOOK_IMAGE?>" />

<!-- CSS Styles + fonts -->

<link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Raleway&display=swap" rel="stylesheet">     
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">

<!-- Favicon -->
<link rel="icon" type="image/png" href="<?= PATH?>assets/images/docker.png" />

