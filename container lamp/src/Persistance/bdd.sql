-- Création de la base de données
CREATE DATABASE IF NOT EXISTS bibliotheque;

-- Table des utilisateurs
CREATE TABLE IF NOT EXISTS users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    user_lastname VARCHAR(50) NOT NULL,
    user_firstname VARCHAR(50) NOT NULL,
    user_email VARCHAR(100) UNIQUE NOT NULL,
    user_password VARCHAR(255) NOT NULL,
    user_registration_date DATE
);

CREATE TABLE roles (
    role_id INT AUTO_INCREMENT PRIMARY KEY,
    role_name VARCHAR(20) NOT NULL
);

CREATE TABLE users_roles (
    user_id INT,
    role_id INT,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

-- Table des livres
CREATE TABLE IF NOT EXISTS books (
    book_id INT AUTO_INCREMENT PRIMARY KEY,
    book_title VARCHAR(100) NOT NULL,
    book_author VARCHAR(100) NOT NULL,
    book_synopsis TEXT(1000) NOT NULL,
    book_release_date DATE,
    book_category VARCHAR(50),
    book_available BOOLEAN NOT NULL
);

-- Table des locations
CREATE TABLE IF NOT EXISTS rental (
    rental_id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    book_id INT,
    rental_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    rental_return_date TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (book_id) REFERENCES books(book_id)
);

-- Exemple d'ajout d'un livre
INSERT INTO livres (book_title, book_author, book_synopsis, book_release_date, book_category, book_available) VALUES
("Dragon ball", "Akira Toryiama", "synopsis", "1987-10-07", "manga", true),
("L'heresie d'horus", "Dan Abnett", "synopsis", "2000-02-08", "roman", true),
("Civil war", "Mark Millar", "synopsis", "2005-04-06", "comics", true),
("Berserk", "Kenturo Miura", "synopsis", "1987-05-11", "manga", true),
("Harry Potter", "J.K Rowling", "synopsis", "2005-10-04", "roman", true),
("Game of thrones", "Georges.R Martin", "synopsis", "2003-03-01", "roman", true),
("Akira", "Katsuhiro Otomo", "synopsis", "1986-02-01", "manga", true),
("Seven Deadly Sins", "Nakaba Suzuki", "synopsis", "2005-03-05", "manga", true),
("Rage", "Stephen King", "synopsis", "1980-05-04", "roman", true),
("Hokuto no Ken", "Buronson, Tetsuo Hara", "synopsis", "1984-05-08", "manga", true),
("Le faucheur", "David Gunn", "synopsis", "2010-02-04", "roman", true),
("Carbone modifié", "Richard Morgan", "synopsis", "2012-04-03", "roman", true),
("Le seigneur des anneaux", "J.R Tolkien", "synopsis", "1970-06-04", "roman", true),
("Sun Ken Rock", "Boichi", "synopsis", "2014-02-03", "manga", true),
("Saint Seiya", "Masami Kurumada", "synopsis", "1983-01-02", "roman", true),
("The Boys", "Garth Ennis", "synopsis", "2011-04-06", "comics", true),
("Preacher", " Garth Ennis", "synopsis", "1990-06-07", "comics", true),
("Batman - La cour des hiboux", "Greg Capullo", "synopsis", "2015-03-04", "comics", true),
("Bleach", "Tite Kubo", "synopsis", "2000-05-07", "manga", true),
("Naruto", "Masashi Kishimoto", "synopsis", "2002-03-07", "manga", true),
("L'attaque des titans", "Hajime Isayama", "synopsis", "2014-05-09", "manga", true);