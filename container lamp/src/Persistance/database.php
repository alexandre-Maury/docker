<?php

class Database extends PDO {

    // Instance unique de la classe
    private static $instance;

    // Informations de connexion :
    private $dbhost = DBHOST;
    private $dbuser = DBUSER;
    private $dbpass = DBPASS;
    private $dbname = DBNAME;

    // Constructeur privé pour empêcher l'instanciation directe
    private function __construct() {
        try {
        
            parent::__construct($this->dbhost . ';dbname=' . $this->dbname, $this->dbuser, $this->dbpass);

            $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        } catch (PDOException $e) {
            throw new PDOException("Erreur de connexion à la base de données : " . $e->getMessage());
        }
    }

    // Instancie la classe, disponible partout
    public static function getInstance(): self {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    // Vous pouvez ajouter d'autres méthodes pour effectuer des opérations sur la base de données
}