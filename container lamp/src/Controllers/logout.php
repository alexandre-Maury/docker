<?php

// Détruisez toutes les variables de session
session_unset();

// Détruisez la session
session_destroy();

// Redirection vers la page de connexion
header('Location: homepage');
exit();

?>