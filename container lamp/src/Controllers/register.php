<?php


require_once 'Models/register.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	// Récupérer les données du formulaire
	$lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '';
	$firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '';
	$email = isset($_POST['email']) ? $_POST['email'] : '';
	$password = isset($_POST['password']) ? $_POST['password'] : '';

	// Valider les données
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
	    // Informations de l'utilisateur à enregistrer (vous pouvez les récupérer depuis un formulaire)
	    $user_lastname = $lastname;
	    $user_firstname = $firstname;
	    $user_email = $email;
	    $user_password = password_hash($password, PASSWORD_DEFAULT); // Hachage du mot de passe

	    // Enregistrement de l'utilisateur
	    $register = new Register();
	    $register->dbUser($user_lastname, $user_firstname, $user_email, $user_password);

	    echo "Utilisateur enregistré avec succès!";
	    
	} else {
	    // L'adresse email n'est pas valide
	    echo "Adresse email non valide!";
	}
}

// Inclure la vue de connexion
$content = 'register.php';
includeView('template.php', ['content' => $content]);
